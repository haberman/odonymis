const MIN_ZOOM = 6
const MAX_ZOOM = 18

const MIN_WAY_LENGTH = 60     // meters

const MIN_PITCH = 0
const MAX_PITCH = 60

const MIN_BEARING = 0
const MAX_BEARING = 360

const MIN_DISTANCE = .1
const MAX_DISTANCE = 10

const ORBIT_VELOCITY = 5        // degrees / second

const FOLLOW_VELOCITY = 8       // meters / second
const FOLLOW_ACCELERATION = 3   // ease factor

const EASING = .01              // less means smoother

const FEATURES_FILE = 'data-pack.json'
const PLACE_TYPES = ['road', 'pedestrian', 'footway', 'recreation_ground', 'grass', 'park', 'residential']

let start, last_frame = 0

let state = 'idle'        // load, follow, jump, orbit
let features = null       // in-memory copy of features' list within FEATURES_FILE
let active_feature = null // feature toward which the camera is aiming
let anim_data = {}        // keeps keys to animate view on top of follow / orbit data
let map = null

let dom_location = null   // keep track of the location `p` to avoid querying it on every frame 

/** Returns a random number between min & max, around a cursor with a given variance. */
const number_random_complex = (min, max, cursor, variance) => {
    if (cursor + variance > max) { cursor = max - variance }
    if (cursor - variance < min) { cursor = min + variance }

    const raw = cursor + (Math.random() - .5) * variance
    return Math.min(Math.max(raw, min), max)
}

/** Returns a random number between `min` & `max` values. */
const number_random_range = (min, max) => Math.random() * (max - min) + min

/** Clamps a number between a min & max value. */
const number_clamp = (input, min, max) => {
    return Math.min(Math.max(input, min), max)
}

/** Returns a random uncrossed feature, optionally being of a certain type. */
const feature_random_uncrossed = type => {
    let fs = features
    if (type !== undefined) { fs = features.filter(f => f.geometry.type === type) }

    const uncrossed = fs.filter(f => !f.properties.hasOwnProperty('crossed'))
    if (uncrossed.length === 0) {
        features.forEach(f => delete f.properties.crossed)
        return feature_random_uncrossed(type)
    }

    return uncrossed[Math.floor(Math.random() * uncrossed.length)]
}

/** Marks the given feature as `crossed` and deletes the `animation` key of its properties. */
const feature_mark_crossed = feature => {
    feature.properties.crossed = true
    delete feature.properties.animation
}

// @TODO
const feature_nearest = (location, type, amount) => {
    const limit = amount || 1

    let uncrossed = filter_crossed(type)
    let nearests = []
    for (let i = 0; i < limit; ++i) { nearest.push(turf.nearestPoint(location, uncrossed)) }

    return nearests
}

/** Inits the active feature and anim_data so the app is ready for animation. */
const feature_init = () => {
    active_feature.properties.animation = {
        _zoom: map.getZoom(),
        _pitch: map.getPitch(),
        _progress: 0
    }

    if (active_feature.geometry.type === 'LineString') {
        active_feature.properties.animation._length = linestring_length(active_feature)
    } else {
        active_feature.properties.animation._distance = number_random_range(MIN_DISTANCE, MAX_DISTANCE)
        active_feature.properties.animation._bearing_direction = Math.random() > .5 ? 1 : -1
    }

    anim_data.zoom = number_random_complex(-6, 6, 0, 6)
    anim_data.pitch = number_random_complex(-MAX_PITCH, MAX_PITCH, MAX_PITCH * .5, MAX_PITCH)
    anim_data.bearing = number_random_complex(MIN_BEARING, MAX_BEARING, map.getBearing(), 180)

    start = performance.now()
    ts = start

    return start
}

/** 
 * Returns a starting point for the given feature.
 * Will modify any feature not being of type `Point` or `LineString` to be a `Point` whose center
 * is the midpoint of its coordinates. */
const point_start = feature => {
    let res = []

    switch (feature.geometry.type) {
        case 'Point':
            res = feature.geometry.coordinates
            break;
        case 'LineString':
            res = feature.geometry.coordinates[0]
            break;
        default:
            const centroid = turf.centroid(feature)
            feature.geometry = centroid.geometry
            res = feature.geometry.coordinates
    }

    return res
}

/** Returns a coordinate along a `LineString` at the `progress` distance of the first point. */
const point_progress = feature => {
    const pts = feature.geometry.coordinates
    const feature_progress = feature.properties.animation._progress

    let length = 0
    for (let i = 0; i < pts.length - 1; ++i) {
        const diff = turf.distance(pts[i], pts[i + 1])
        length += diff
        if (length >= feature_progress) {
            const bearing = turf.bearing(pts[i], pts[i + 1])
            const distance = diff - (length - feature_progress)

            return turf.destination(pts[i], distance, bearing).geometry.coordinates
        }
    }

    return pts[0]
}

/** Returns the length of a `LineString` in kilometers */
const linestring_length = feature => {
    const pts = feature.geometry.coordinates

    let length = 0
    for (let i = 0; i < pts.length - 1; ++i) { length += turf.distance(pts[i], pts[i + 1]) }

    return length
}


/** Starts orbiting around active feature's center. */
const camera_orbit = (ts) => {
    if (ts === undefined) { ts = feature_init() }

    const _progress = active_feature.properties.animation._progress
    const _rotation = active_feature.properties.animation._bearing_direction
    if (_progress > anim_data.bearing) {
        camera_jump()
        return
    }

    const unit_progress = _rotation * _progress / 360
    const center = active_feature.geometry.coordinates
    const around = turf.destination(center, active_feature.properties.animation._distance, unit_progress * 360).geometry.coordinates

    const pitch = active_feature.properties.animation._pitch + unit_progress * anim_data.pitch
    const zoom = active_feature.properties.animation._zoom + unit_progress * anim_data.zoom

    map.jumpTo({
        center: center,
        around: around,
        bearing: turf.bearing(around, center),
        pitch: number_clamp(pitch, MIN_PITCH, MAX_PITCH),
        zoom: number_clamp(zoom, MIN_ZOOM, MAX_ZOOM)
    })

    const speed = (ts - last_frame) * ORBIT_VELOCITY * Math.pow(10, -3) // d/ms

    active_feature.properties.animation._progress += speed
    last_frame = ts
    requestAnimationFrame(camera_orbit)
}

/** Starts following active feature's along its way. */
const camera_follow = ts => {
    if (ts === undefined) { ts = feature_init() }

    const _length = active_feature.properties.animation._length
    const _progress = active_feature.properties.animation._progress

    if (_progress > _length) {
        camera_jump()
        return
    }

    const unit_progress = _progress / _length
    const center = point_progress(active_feature)
    const map_center = [map.getCenter().lng, map.getCenter().lat]

    const lng = map_center[0] + (center[0] - map_center[0]) * EASING
    const lat = map_center[1] + (center[1] - map_center[1]) * EASING

    const bearing = turf.bearing(map_center, center) + unit_progress * anim_data.bearing
    const zoom = active_feature.properties.animation._zoom + unit_progress * anim_data.zoom
    const pitch = active_feature.properties.animation._pitch + unit_progress * anim_data.pitch

    map.jumpTo({
        center: [lng, lat],
        bearing: bearing,
        zoom: zoom,
        pitch: pitch
    })

    dom_update_location(lat, lng)

    // frame independant speed (half v constant + half v (0-1) easing)
    const acceleration = FOLLOW_VELOCITY * .5 + Math.sin(Math.PI * unit_progress) * FOLLOW_VELOCITY * .5
    const speed = (ts - last_frame) * acceleration * Math.pow(10, -6) // m/ms
    active_feature.properties.animation._progress += speed

    last_frame = ts
    requestAnimationFrame(camera_follow)
}

/** Updates the displayed location. */
const dom_update_location = (lat, lng) => {
    dom_location.innerHTML = `${lat.toFixed(5)}<br/>${lng.toFixed(5)}`
}

/** Updates the displayed location. */
const dom_update_address = properties => {
    dom_place.innerHTML = properties.place

    if (properties.county) { dom_place.innerHTML += `, ${properties.county}` }
    dom_country.innerHTML = properties.country
}

/** Loads a new active feature  */
const camera_jump = () => {
    state = 'jump'

    if (active_feature !== null) { feature_mark_crossed(active_feature) }
    active_feature = feature_random_uncrossed()

    const map_center = [map.getCenter().lng, map.getCenter().lat]
    const start_center = point_start(active_feature)
    const bearing = turf.bearing(map_center, start_center)

    map.once('moveend', map_moved_end)
    map.jumpTo({
        center: start_center,
        zoom: number_random_complex(MAX_ZOOM - 4, MAX_ZOOM, map.getZoom(), 4),
        pitch: Math.random() * MAX_PITCH,
        bearing: bearing
    })
}

/** Callbacks for when the map has reached a destination. */
const map_moved_end = () => {
    dom_update_address(active_feature.properties)

    if (active_feature.geometry.type === 'LineString') {
        state = 'camera_follow'
        camera_follow()
    } else {
        state = 'camera_orbit'
        camera_orbit()

        const center = active_feature.geometry.coordinates
        dom_update_location(center[0], center[1])
    }
}

/** Callbacks for when the map is loaded. */
const map_loaded = async () => {
    document.querySelector('.mapboxgl-ctrl-bottom-left').remove() // a probable CGU infringement...

    const features_collection = await xhr_call(FEATURES_FILE)

    // debug
    features = features_collection['features']
        .filter(f => f.geometry.type === 'Point')
    // .sort((a, b) => linestring_length(a) > linestring_length(b))
    features.splice(5, features.length - 5)

    // all those that are not 'LineString' & all those `LineString` being long enough
    // features = features_collection['features']
    //     .filter(f => f.geometry.type !== 'LineString' || linestring_length(f) > MIN_WAY_LENGTH * .001)

    camera_jump()
}

/** Inits the application once the DOM is loaded. */
document.addEventListener('DOMContentLoaded', async () => {
    dom_location = document.querySelector('p.location')
    dom_place = document.querySelector('p.place')
    dom_country = document.querySelector('p.country')

    mapboxgl.accessToken = MAPBOX_TOKEN // should be found inside private.js
    state = 'load'

    map = new mapboxgl.Map({
        container: 'map',
        style: MAPBOX_STYLE,
        attributionControl: false
    })

    map.once('load', map_loaded)
})