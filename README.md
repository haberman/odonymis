For now, data exhibited here relate to all the things we humans have named "peace", geographically speaking. Yet, at the core of this is a python tool that could be used to query anything.

## geolinguae/
### Python CLI to automate the geocoding of a given query in different langs and countries.
The query is translated in desired languages using Google Translate service, hence a `private.py` has to be created whose content should be:
```python
GOOGLE_API_KEY = 'your_private_key'
GEONAMES_USERNAME = 'your_geonames_username'
```
Gets a geoname by [signing up](http://www.geonames.org/login) (or simply use `'demo'`), that free API is used to get the list of all countries in the world. The actual geocoding is made using [nominatim](https://wiki.openstreetmap.org/wiki/Nominatim) which requires no credentials.

```bash
usage: ./geolinguae.py [-h] [-v] [-c] [-s SOURCE] [-l LANG] [-t TYPE] query

Automates the geocoding of a given query in different langs and countries.

positional arguments:
  query                 the query to geocode
optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity (default: False)
  -p, --pack            packs all features into a single geojson (default: False)
  -s SOURCE, --source SOURCE
                        sets the language code of the query, the results localization and initial
                        countries where to perform geocoding (default: fr)
  -l LANG, --lang LANG  a comma separated list of values to extend the query in coutries where an
                        other lang is spoken. When following a continent (allcaps) and preceeding a
                        lang (smallcaps): `-` means exclude (default: None)
  -c COUNTRIES, --countries COUNTRIES
                        filters results within a comma separated list of countries\' iso codes.
                        Validation uses the same rules as the --lang option. (default: None)
  -t TYPE, --type TYPE  a comma separated list of values to get results of a certain type of OSM
                        relation (default: None)
```

#### Examples:
Gets all *rue de la paix* in the world in coutries where *french* (default `source` argument value) and *italian* (`-l` option) are officially spoken and be verbose about what's going on.

`./geolinguae.py -v -l it "rue de la paix"`

Gets all *place de la paix* for all languages spoken in Europe except where it's english or french:

`./geolinguae.py -v -l EU-en-fr "place de la paix"`

Gets all *avenue de la paix* where French, Dutch and Italian are spoken, but only in France, Belgium or Italy, disambiguation is made automatically:

`./geolinguae.py -v -l fr,nl,it -c FR,BE,IT "de la paix"`

The geocode automation loops over countries, this allows to compare languages that are officially spoken and prevent geocoding places in wrong languages.

#### Cache
The cache is made solely of json files:
- `countries_{}.json` lists all populated countries with an official language, wolrdwide, localized;
- `translatable_{}.json` lists languages that Google can translate from a query made in the `{}` iso code language;
- `translations.json` keeps track of already translated queries.

Rule for naming geojson features extraction are:
- per-file `query_countryId.json` in cache (with `query` being translated in the target language);
- and `query_timestamp.json` when packed.

#### TODO's
- [x] gets rid of the `-c` option, hence making cache mandatory;
- [x] implements the `--country` filter argument.
- [x] generates a valid geojson output;
- [x] packs features in a single file;
- [x] demo a simple *proof of concept* client map, testing data sanity;
- [ ] implements the `--type` filter argument.

## map/
### A constantely moving map that travels over features of a `geojson` file.
This folder contains the fully document source code for the work of art *Sideways of peace*.
An untracked `private.js` file should be created so it contains a single key:
```javascript
MAPBOX_TOKEN = 'your_token'
```
The `data-pack.json` is provided as a demo file.