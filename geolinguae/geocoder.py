# import csv
# import codecs

import urllib.request as ureq
import urllib.parse as uparse

import xml.dom.minidom as dom

# from SPARQLWrapper import SPARQLWrapper, JSON


def dom_text(node_list):
    rc = []
    for node in node_list.childNodes:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)


def dom_float(node_list):
    return float(dom_text(node_list))


def dom_int(node_list):
    return int(dom_text(node_list))


def dom_bbox(node_list):
    return [
        dom_float(node_list.getElementsByTagName('west')[0]),   # min x
        dom_float(node_list.getElementsByTagName('south')[0]),  # min y
        dom_float(node_list.getElementsByTagName('east')[0]),   # max x
        dom_float(node_list.getElementsByTagName('north')[0])]  # max y

# 0	"48.5885798"
# 1	"48.589906"
# 2	"7.7549282"
# 3	"7.7558145"


def format_nominatim_bbox(bbox):
    return [bbox[2], bbox[0], bbox[3], bbox[1]]


def features_in_country(query, country_code, i18n='fr'):
    """Geocodes a given query inside a specific country."""
    url = 'https://nominatim.openstreetmap.org/search?{}'.format(
        uparse.urlencode({
            'q': query,
            'accept-language': i18n,
            'countrycodes': country_code,
            'dedupe': 1,
            'addressdetails': 1,
            'polygon_geojson': 1,
            'format': 'json',
            'limit': 100
        }))

    req = ureq.urlopen(url)
    res = req.read().decode('utf-8')

    return res


def features_in_bbox(query, bbox, i18n='fr'):
    """Geocodes a query inside a specific w,n,e,s bounding box."""
    url = 'https://nominatim.openstreetmap.org/search?{}'.format(
        uparse.urlencode({
            'q': query,
            'viewbox': ','.join(map(str, bbox)),
            'accept-language': i18n,
            'polygon_geojson': 1,
            'bounded': 1,
            'dedupe': 1,
            'format': 'json',
            'maxRows': 100
        }))

    req = ureq.urlopen(url)
    res = req.read().decode('utf-8')

    return res


def features_to_geojson(features):
    """Transcribes a list of geocoded features to geojson data."""
    geojson = {'type': 'FeatureCollection', 'features': []}

    for f in features:
        feature = {'type': 'Feature',
                   'geometry': f['geojson'],
                   'bbox': format_nominatim_bbox(f['boundingbox']),
                   'properties': {
                       'id': f['osm_id'],
                       'place': f['display_name'].split(',')[0],
                       'country': f['address']['country'],
                       'type': f['type'],
                       'class': f['class']
                   }}

        try:
            feature['properties']['county'] = f['address']['county']
        except KeyError:
            pass

        try:
            feature['properties']['postcode'] = f['address']['postcode']
        except KeyError:
            pass

        geojson['features'].append(feature)

    return geojson


def country_info(username, i18n='fr'):
    """
    Fetches the geonames [countryInfo](http://ws.geonames.org/countryInfoCSV?lang=i18n), localized,
    and returns a more consistent (lighter) `dict` representation of it.

    Arguments
    username   -- required geonames.org user accout name
    i18n       -- the language in wich results should be translated

    Keys of the returned dict are: id, code, name, capital, area, population, continent, bbox
    Countries with a population of 0 or no language are omitted.
    """

    url = 'http://api.geonames.org/countryInfo?lang={}&username={}'.format(i18n, username)
    req = ureq.urlopen(url)
    res = dom.parseString(req.read().decode('utf-8'))
    req.close()

    countries = []
    for c in res.getElementsByTagName('country'):
        population = dom_int(c.getElementsByTagName('population')[0])
        languages = dom_text(c.getElementsByTagName('languages')[0])

        if not population or not languages:
            continue

        countries.append({
            'id': dom_text(c.getElementsByTagName('geonameId')[0]),
            'code': dom_text(c.getElementsByTagName('countryCode')[0]),
            'name': dom_text(c.getElementsByTagName('countryName')[0]),
            'capital': dom_text(c.getElementsByTagName('capital')[0]),
            'area': dom_float(c.getElementsByTagName('areaInSqKm')[0]),
            'population': population,
            'languages': languages,
            'continent': dom_text(c.getElementsByTagName('continent')[0]),
            'bbox': dom_bbox(c)
        })

    return countries
